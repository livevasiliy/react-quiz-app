import axios from 'axios'

export default axios.create({
    baseURL: 'https://react-quiz-531e4.firebaseio.com'
})